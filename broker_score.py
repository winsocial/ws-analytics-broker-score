import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import Pipeline
from numpy.polynomial import Polynomial
from sqlalchemy import create_engine
from sqlalchemy.pool import NullPool
from scipy.optimize import curve_fit


SERVER = config['SERVER']
PORT = config['PORT']
DATABASE = config['DATABASE']
USERNAME = config['USERNAME']
PASSWORD = config['PASSWORD']


def fit_exp(x, a, b):
    return a * np.exp(-b*x)


def fit_linear(x, a, b):
    return a + b*x


MAX_AGE = 180


idades_vendas_produtor = """
SELECT
	producer_id,
	`leads_idades`.`idade_da_venda`,
	COUNT(`leads_idades`.`idade_da_venda`) as `qtdade`
FROM
(
select
	producer_id,
    id,
    DATE(created_at) as `data_da_criacao`,
    qtdade_vendas.`data_da_venda`,
    qtdade_finalizados.`data_da_finalizacao`,
    DATEDIFF(CURRENT_DATE(), created_at) AS `idade_do_lead`,
    DATEDIFF(qtdade_vendas.`data_da_venda`, DATE(created_at)) AS `idade_da_venda`
from leads
left outer join
    (select
        events.lead_id,
        DATE(created_at) as `data_da_venda` from events
    inner join (
        select
            max(id) as `id`,
            lead_id
        from events
        where `type` = 'proposal_created'
        group by lead_id) as `vendas`
        on events.id = `vendas`.id) as `qtdade_vendas`
on leads.id = qtdade_vendas.lead_id
left outer join
    (select
        events.lead_id,
        DATE(events.created_at) as `data_da_finalizacao`
    from events
    inner join (
        select
            max(id) as `id`,
            lead_id
        from events
        where ((`type` = 'updated') AND (`value` ->> '$.status' = 'done'))
        group by lead_id) as `data_finalizacao`
        on events.id = `data_finalizacao`.id) as `qtdade_finalizados`
on leads.id = qtdade_finalizados.lead_id
where campaign_id = 1
and producer_id is not null
and producer_id not like '11%'
and DATE(created_at) >= '2020-04-01'
having `idade_do_lead` >= """ + str(MAX_AGE) + """ AND `idade_da_venda` IS NOT NULL) AS `leads_idades`
GROUP BY producer_id, `leads_idades`.`idade_da_venda`
ORDER BY producer_id
"""
leads_produtor = """
SELECT
	producer_id,
	COUNT(id) as `qtdade_leads`
FROM leads
WHERE 
	campaign_id = 1
	and producer_id is not null
	and producer_id not like '11%'
GROUP BY `producer_id`
"""
produtores = """SELECT DISTINCT producer_id FROM leads WHERE producer_id NOT LIKE '11%'
AND DATE(leads.created_at) >= '2020-04-01' AND leads.campaign_id = 1"""

engine = create_engine(
        'mysql+mysqlconnector://' + USERNAME + ':' + PASSWORD + '@' + SERVER + ':25060/' + DATABASE,
        poolclass=NullPool)
conn = engine.connect()
df_idades_vendas_produtor = pd.read_sql(idades_vendas_produtor, conn)
df_leads_produtor = pd.read_sql(leads_produtor, conn)
df_produtores = pd.read_sql(produtores, conn)
conn.close()

age_range = range(0, MAX_AGE + 1)
produtor_age_range = []

for produtor in df_produtores['producer_id'].to_list():
    for age in age_range:
        item = {'producer_id': produtor, 'age': age}
        produtor_age_range.append(item)

df_produtores_idades = pd.DataFrame(produtor_age_range)

df = pd.merge(df_produtores_idades, df_idades_vendas_produtor,
              left_on=['producer_id', 'age'],
              right_on=['producer_id', 'idade_da_venda'],
              how='left')
df = pd.merge(df, df_leads_produtor,
              on='producer_id',
              how='left')
df['tx_conv'] = df['qtdade'] / df['qtdade_leads']

model_data = df_idades_vendas_produtor.groupby('idade_da_venda', as_index=False)[['qtdade']].sum()
x = model_data['idade_da_venda'].values
y = model_data['qtdade'].values

exp_popt = np.array([99.55, 0.068])
exp_model_data = fit_exp(range(1, 181), *exp_popt)

linear_popt = np.array([60, 33])
linear_model_data = fit_linear(range(0, 2), *linear_popt)

plt.figure(figsize=(10, 6))
plt.scatter(x, y, s=15, label='Observations')
plt.plot(range(1, 181), exp_model_data, color='r')
plt.plot(range(0, 2), linear_model_data, color='r', label='linear + negative exponential')
plt.xlabel('Predictor', fontsize=16)
plt.ylabel('Target', fontsize=16)
plt.legend()
plt.show()


##### PARA UM CORRETOR ESPECÍFICO
df_producer_id_rank = df.groupby('producer_id', as_index=False)[['qtdade']].sum()
df_producer_id_rank = df_producer_id_rank.sort_values('qtdade', ascending=False)
best_broker = df_producer_id_rank.head(1)['producer_id'].values.item()
#'7269575'
broker = df[df['producer_id'] == '7269575'].copy()

# generate benchmark data

# remove first index from exp_model

exp_model_data = np.delete(exp_model_data, 0)
benchmark = np.concatenate([linear_model_data, exp_model_data])
benchmark_tx_conv = (benchmark / 10962)

scores = []

for broker in df.producer_id.unique():
    # total de leads analisados: 10962
    broker_df = df[df['producer_id'] == broker].copy()

    # se não fez nenhuma venda o score vai ser atribuido arbitrariamente em outra etapa
    if broker_df['qtdade'].sum() == 0:
        continue
    else:
        # tx de conversão do modelo
        broker_df['benchmark'] = benchmark_tx_conv

        # diferença entre a conversao do modelo e o corretor
        broker_tx_conv_diff = broker_df['tx_conv'] - broker_df['benchmark']

        # soma das diferenças. Quanto mais próximo de 0, melhor.
        score = broker_tx_conv_diff.sum()
    scores.append(score)
    print(broker)
    print(score)

scores_df = pd.DataFrame(data={'scores':scores})

plt.figure(figsize=(10, 6))
plt.boxplot(scores)
plt.show()


