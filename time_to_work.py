import datetime
import pytz
import businesstimedelta

import holidays as pyholidays
import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.pool import NullPool
from config import config


SERVER = config['SERVER']
PORT = config['PORT']
DATABASE = config['DATABASE']
USERNAME = config['USERNAME']
PASSWORD = config['PASSWORD']


eventos = """
SELECT
    events.id,
    events.created_at,
    events.lead_id,
    events.`type` AS `type`,
    CASE
    	WHEN `value` ->> '$.to' IS NOT NULL THEN `value` ->> '$.to'
    	WHEN `value` ->> '$.to' IS NULL AND `value` ->> '$.producer_id' IS NOT NULL THEN `value` ->> '$.producer_id'
    	ELSE `value` ->> '$.to' END AS `assigned_producer_id`,
    CASE
    	WHEN `value` ->> '$.status' IS NULL THEN 'no_change'
    	ELSE `value` ->> '$.status' END AS `status`,
    `value` ->> '$.producer_id' AS `updated_producer_id`
FROM events
INNER JOIN lead_producer
ON lead_producer.lead_id = events.lead_id AND lead_producer.campaign_id = 1
WHERE
    (`type` = 'updated' AND `value` ->> '$.status' IN ('working','contact','consulting','scheduled'))
    OR
    (`type` IN ('created', 'assigned', 'updated', 'viewed'))
HAVING events.created_at >= '2020-10-01'
ORDER BY events.created_at
"""


engine = create_engine(
        'mysql+mysqlconnector://' + USERNAME + ':' + PASSWORD + '@' + SERVER + ':25060/' + DATABASE,
        poolclass=NullPool)
conn = engine.connect()
df_eventos = pd.read_sql(eventos, conn)
conn.close()
data = df_eventos.copy()
data = data.sort_values('created_at')
data = data[~data['status'].isin(['queue', 'done', 'dormant', 'available'])].copy()

lead_history = []
for lead in data.lead_id.unique():

    lead_data = data[data['lead_id'] == lead].copy()
    lead_data['assigned_producer_id'] = lead_data['assigned_producer_id'].fillna(method='ffill')
    assigned_producers = lead_data['assigned_producer_id'].unique()

    for producer_id in assigned_producers:

        lead_assigned_idxs = lead_data[(lead_data['assigned_producer_id'] == producer_id) &
                                       (lead_data['status'] == 'no_change')].index.values

        if lead_assigned_idxs.size == 0:
            continue
        else:
            first_assigned_idx = min(lead_assigned_idxs)

        lead_working_idxs = lead_data[(lead_data['status'].isin(['working', 'contact', 'consulting', 'scheduled'])) &
                                      (lead_data.index.values > first_assigned_idx) &
                                      (lead_data['assigned_producer_id'] == producer_id)].index.values
        if lead_working_idxs.size == 0:
            continue
        else:
            first_working_idx = min(lead_working_idxs)

        lead_viewed_idxs = lead_data[(lead_data['type'] == 'viewed') &
                                     (lead_data.index.values > first_working_idx)].index.values
        if lead_viewed_idxs.size == 0:
            continue
        else:
            first_viewed_idx = min(lead_viewed_idxs)

        producer_lead_history = dict()
        producer_lead_history['lead_id'] = lead
        producer_lead_history['producer_id'] = producer_id
        producer_lead_history['assigned_at'] = lead_data.loc[first_assigned_idx]['created_at']
        producer_lead_history['working_at'] = lead_data.loc[first_working_idx]['created_at']
        producer_lead_history['viewed_at'] = lead_data.loc[first_viewed_idx]['created_at']

        lead_history.append(producer_lead_history)

df = pd.DataFrame(lead_history)

keep_idxs = list()
i = 0
for working_at in df['working_at'].unique():
    duplicated_time_df = df[df['working_at'] == working_at]
    idx = duplicated_time_df.index.values
    if (idx.size > 1) and (duplicated_time_df['lead_id'].unique().size == 1):
        keep_idxs.append(max(idx))
    else:
        keep_idxs = keep_idxs + list(idx)
df_time_to_work = df.loc[keep_idxs]

america_sao_paulo_tz = pytz.timezone('America/Sao_Paulo')
# Define a working day
workday = businesstimedelta.WorkDayRule(
    start_time=datetime.time(9),
    end_time=datetime.time(18),
    working_days=[0, 1, 2, 3, 4],
    tz=america_sao_paulo_tz)

br_holidays = pyholidays.BR()
holidays = businesstimedelta.HolidayRule(br_holidays)
businesshrs = businesstimedelta.Rules([workday, holidays])

for column in ['working_at', 'assigned_at', 'viewed_at']:
    df_time_to_work[column] = df_time_to_work[column].apply(lambda x: america_sao_paulo_tz.localize(x))

df_time_to_work['w_timediff'] = df_time_to_work[['working_at', 'assigned_at']]\
    .apply(lambda x: businesshrs.difference(x['working_at'], x['assigned_at']),  axis=1)
df_time_to_work['w_timediff'] = df_time_to_work['w_timediff'].apply(lambda x: (x.hours * 60) + (x.seconds / 60))

df_time_to_work['v_timediff'] = df_time_to_work['viewed_at'] - df_time_to_work['working_at']
df_time_to_work['v_timediff'] = df_time_to_work['v_timediff'].apply(lambda x: (x.total_seconds() / 60))

df_broker_time_to_work = df_time_to_work.groupby('producer_id', as_index=False)['w_timediff'].agg('mean')
df_broker_time_to_work = df_broker_time_to_work.sort_values('w_timediff', ascending=False)
df_broker_time_to_work.head(30)
deciles = pd.qcut(df_broker_time_to_work['w_timediff'], 10, range(1,11), retbins=True)
